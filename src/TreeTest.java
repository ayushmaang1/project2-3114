/**
 * Tree test class for methods not tested already
 * @author Bryan Covell (bpc6), Ayush Ganotra (ayushg)
 * @version March 09, 2020
 *
 */
public class TreeTest extends student.TestCase {
    private Tree tree;
    
    /**
     * Sets up the following test cases
     */
    public void setUp() {
        tree = new Tree();
    }
    
    /**
     * Cases for testing search()
     */
    public void testSearch() {
        assertEquals(-1, tree.search("A"));
        assertEquals(-1, tree.search("$"));
        
        tree.insert("A");
        
        assertEquals(1, tree.search("A$"));
        assertEquals(1, tree.search("A"));
        assertEquals(-1, tree.search("C"));
        assertEquals(-1, tree.search("C$"));
        
        tree.insert("C");
        
        assertEquals(2, tree.search("A$"));
        assertEquals(-1, tree.search("G$"));
        assertEquals(-1, tree.search("AA"));
        
        tree.insert("AA");
        
        assertEquals(3, tree.search("A$"));
        assertEquals(-1, tree.search("G$"));
        assertEquals(-1, tree.search("AC"));
        assertEquals(3, tree.search("AA"));
    }
    
    
    /**
     * Extra test case for the insert() command
     */
    public void testInsert() {
        assertEquals(0, tree.insert("A"));
        assertEquals(-1, tree.insert("A"));
    }
    
    /**
     * Extra test case for the remove() command
     */
    public void testRemove() {
        tree.insert("A");
        tree.insert("C");
        
        
        assertTrue(tree.remove("A"));
        assertFalse(tree.remove("A"));
    }
}
