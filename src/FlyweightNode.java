/**
 * The FlyweightNode has nothing. It's a node with nothing...
 * 
 * @author Bryan Covell (bpc6), Ayush Ganotra (ayushg)
 * @version March 09, 2020
 *
 */
public class FlyweightNode extends DNATreeNode {
    //Nothing to add here
}
