/**
 * The DNATreeNode class manages base nodes that the DNA tree will have
 * 
 * @author Bryan Covell (bpc6), Ayush Ganotra (ayushg)
 * 
 * @version 2020-03-13
 */
public class DNATreeNode {
    private int level;


    /**
     * Gets the level of the node
     * 
     * @return - the level
     */
    public int getLevel() {
        return level;
    }


    /**
     * Sets level of node
     * 
     * @param level
     *            - new level
     */
    public void setLevel(int level) {
        this.level = level;
    }

}
