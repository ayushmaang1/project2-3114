// On my honor:
//
// - I have not used source code obtained from another student, or any other
// unauthorized source, either modified or
// unmodified.
// - All source code and documentation used in my program is either my
// original work, or was derived by me from the
// source code published in the textbook for this course.
// - I have not discussed coding details about this project with anyone other
// than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts of this
// program with other students, and that another student may help me debug my
// program so long as neither of us writes anything during the discussion or
// modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.

import java.io.*;
import java.util.*;

/**
 * Driver class that runs the project
 * 
 * @author Bryan Covell (bpc6), Ayush Ganotra (ayushg)
 * @version March 09, 2020
 */
public class DNAtree {

    private static Tree tree;

    /**
     * Parses command scripts and prints output.
     * 
     * @param args
     *            Arguments from command line call
     * @throws FileNotFoundException
     *             If given file does not exist
     */
    public static void main(String[] args) throws FileNotFoundException {
// PrintStream out = new PrintStream(new FileOutputStream("output.txt"));
// System.setOut(out);
        File filename = new File(args[0]);
        Scanner scan = new Scanner(filename);
        tree = new Tree();

        // Scans the file and calls the appropriate commands
        while (scan.hasNextLine()) {
            String commandLine = scan.nextLine();
            if (commandLine.equals("remove AATTAACT")) {
                System.out.print("");
            }
            if (commandLine.equals("remove AAGAG")) {
                System.out.print("");
            }
            commandLine = commandLine.trim();
            String[] commandArray = commandLine.split("\\s+");
            if (commandArray[0].contentEquals("insert")) {
                insert(commandArray[1]);
            }
            else if (commandArray[0].contentEquals("remove")) {
                remove(commandArray[1]);
            }
            else if (commandArray[0].contentEquals("search")) {
                search(commandArray[1]);
            }
            else if (commandArray[0].contentEquals("print")) {
                if (commandArray.length == 1) {
                    print();
                }
                else if (commandArray[1].contentEquals("lengths")) {
                    printLengths();
                }
                else if (commandArray[1].contentEquals("stats")) {
                    printStats();
                }
            }

        }
        scan.close();
    }


    /**
     * Checks that the given sequence contains only the chars A, C, G, or T
     * 
     * @param seq
     *            the sequence to be checked
     * @return true if the sequence is acceptable
     */
    private static boolean check(String seq) {
        for (int i = 0; i < seq.length(); i++) {
            if (seq.charAt(i) != 'A' && seq.charAt(i) != 'C' && seq.charAt(
                i) != 'G' && seq.charAt(i) != 'T') {
                System.out.println("sequence " + seq + " is invalid");
                return false;
            }
        }
        return true;
    }


    /**
     * Checks that the given sequence contains only the chars A, C, G, T, or $
     * 
     * @param seq
     *            the sequence to be checked
     * @return true if the sequence is acceptable
     */
    private static boolean checkEnd(String seq) {
        for (int i = 0; i < seq.length(); i++) {
            if (seq.charAt(i) != 'A' && seq.charAt(i) != 'C' && seq.charAt(
                i) != 'G' && seq.charAt(i) != 'T' && seq.charAt(i) != '$') {
                System.out.println("sequence " + seq + " is invalid");
                return false;
            }
        }
        return true;
    }


    /**
     * Inserts the sequence into the tree and prints corresponding output
     * 
     * @param seq
     *            The sequence being inserted
     */
    public static void insert(String seq) {
        if (check(seq)) {
            int level = tree.insert(seq);
            if (level == -1) {
                System.out.println("sequence " + seq + " already exists");
            }
            else {
                System.out.println("sequence " + seq + " inserted at level "
                    + level);
            }

        }
    }


    /**
     * Removes the sequence from the tree and prints corresponding output
     * 
     * @param seq
     *            The sequence being removed
     */
    public static void remove(String seq) {
        if (check(seq)) {
            boolean removed = tree.remove(seq);
            if (removed) {
                System.out.println("sequence " + seq + " removed");
            }
            else {
                System.out.println("sequence " + seq + " does not exist");
            }
        }
    }


    /**
     * Searches the tree for the sequence and prints corresponding output
     * 
     * @param seq
     *            The sequence being searched for
     */
    public static void search(String seq) {
        if (checkEnd(seq)) {
            tree.search(seq);
        }
    }


    /**
     * Dumps the node structure and sequences in the tree
     */
    public static void print() {
        System.out.println("tree dump:");
        tree.print();
    }


    /**
     * Includes the length of each sequence in the dump
     */
    public static void printLengths() {
        System.out.println("tree dump:");
        tree.printLengths();
    }


    /**
     * Includes the percent of each char in the dump
     */
    public static void printStats() {
        System.out.println("tree dump:");
        tree.printStats();
    }
}
