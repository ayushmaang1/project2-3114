/**
 * The TreeNode class manages every node that the DNA tree will have
 * 
 * @author Bryan Covell (bpc6), Ayush Ganotra (ayushg)
 * @version March 09, 2020
 *
 */
public class TreeNode {
    private String value;
    private TreeNode[] leaves = { null, null, null, null, null };
    private boolean isLeaf = true;


    /**
     * Constructor to create a tree node. All leaves are set to null by default
     * and the node is a leaf.
     * 
     * @param val value to go in the node
     */
    public TreeNode(String val) {
        value = val;
    }


    /**
     * Get's the value of the node
     * 
     * @return value of node
     */
    public String getVal() {
        return value;
    }


    /**
     * Set's value of the node
     * 
     * @param val
     *            - the new value
     */
    public void setVal(String val) {
        value = val;
    }


    /**
     * Get's the A sequence
     * 
     * @return the first index of leaves
     */
    public TreeNode getA() {
        return leaves[0];
    }


    /**
     * Get's the C sequence
     * 
     * @return the second index of leaves
     */
    public TreeNode getC() {
        return leaves[1];
    }


    /**
     * Get's the G sequence
     * 
     * @return the third index of leaves
     */
    public TreeNode getG() {
        return leaves[2];
    }


    /**
     * Get's the T sequence
     * 
     * @return the fourth index of leaves
     */
    public TreeNode getT() {
        return leaves[3];
    }


    /**
     * Get's the $
     * 
     * @return the last index of leaves
     */
    public TreeNode getEnd() {
        return leaves[4];
    }


    /**
     * Set's the A sequence
     * 
     * @param a
     *            - new sequence for the first index of leaves
     */
    public void setA(TreeNode a) {
        leaves[0] = a;
    }


    /**
     * Set's the C sequence
     * 
     * @param c
     *            - new sequence for the second index of leaves
     */
    public void setC(TreeNode c) {
        leaves[0] = c;
    }


    /**
     * Set's the G sequence
     * 
     * @param g
     *            - new sequence for the third index of leaves
     */
    public void setG(TreeNode g) {
        leaves[0] = g;
    }


    /**
     * Set's the T sequence
     * 
     * @param t
     *            - new sequence for the fourth index of leaves
     */
    public void setT(TreeNode t) {
        leaves[0] = t;
    }


    /**
     * Set's the $ sequence
     * 
     * @param val
     *            - new sequence for the last index of leaves
     */
    public void setEnd(TreeNode val) {
        leaves[0] = val;
    }


    /**
     * Determines whether the node is a leaf
     * 
     * @return leaf boolean value
     */
    public boolean getLeaf() {
        return isLeaf;
    }


    /**
     * Set's the leaf value, change if the node is internal
     * 
     * @param leaf
     *            - the new value
     */
    public void setLeaf(boolean leaf) {
        this.isLeaf = leaf;
    }

}
