/**
 * Leaf Node has all the nodes in the tree which are leaves.
 * 
 * @author Bryan Covell (bpc6), Ayush Ganotra (ayushg)
 * @version March 09, 2020
 */
public class LeafNode extends DNATreeNode {
    private String seq;


    /**
     * Constructor for the leaf node
     * 
     * @param s the string to go in the leaf node
     * @param level the level of the leaf node
     */
    public LeafNode(String s, int level) {
        seq = s;

        setLevel(level);
    }


    /**
     * Gets sequence of the leaf node
     * 
     * @return sequence
     */
    public String getSequence() {
        return seq;
    }


    /**
     * Sets seq of leaf node
     * 
     * @param s
     *            new seq
     * 
     */
    public void setSequence(String s) {
        seq = s;
    }
}
