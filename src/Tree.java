
/**
 * Main Tree class that handles all the DNA Tree.
 * 
 * @author ayushmaanganotra
 * @version 2020-03-13
 *
 */
public class Tree {

    private DNATreeNode root;
    private FlyweightNode fw;

    /**
     * Constructor, creates the tree
     */
    public Tree() {
        fw = new FlyweightNode();
        root = fw;
    }


    /**
     * Inserts a sequence
     * 
     * @param seq
     *            - string sequence
     * @return the level the sequence was placed in
     */
    public int insert(String seq) {
        // if there is only one node
        if (root instanceof LeafNode) {
            // first check if the seq isn't a duplicate
            if (((LeafNode)root).getSequence().equals(seq)) {
                return -1;
            }

            char first = ((LeafNode)root).getSequence().charAt(0);
            InternalNode internal = new InternalNode(fw, 0);
            internal.addNode(root, first);
            root.setLevel(1);
            root = internal;
        }

        // basically if the tree is empty
        else if (root instanceof FlyweightNode) {
            root = new LeafNode(seq, 0);
            return 0;
        }

        return insertHelp(seq, (InternalNode)root);

    }


    /**
     * Helper method for insert
     * 
     * @param seq
     *            The string to be inserted
     * @param cur
     *            the current node in the recursive call
     * @return the level the node was inserted at
     */
    public int insertHelp(String seq, InternalNode cur) {
        char pos; // the pos will allow us to know where to add the new seq
        if (cur.getLevel() == seq.length()) {
            pos = '$';
        }
        else {
            pos = seq.charAt(cur.getLevel());
        }

        DNATreeNode temp = cur.getNode(pos);

        // flynode case - simply adds the seq to that node
        if (temp instanceof FlyweightNode) {
            cur.addNode(new LeafNode(seq, cur.getLevel() + 1), pos);
            return cur.getLevel() + 1;
        }

        // leafnode case
        else if (temp instanceof LeafNode) {

            // check to not allow duplicates
            if (((LeafNode)temp).getSequence().equals(seq)) {
                return -1;
            }

            // first create an internal node that will replace the leaf node
            InternalNode internal = new InternalNode(fw, temp.getLevel());

            // next add the leaf node to the internal node
            char oldPos;

            if (((LeafNode)temp).getSequence().length() == temp.getLevel()) {
                oldPos = '$';
            }
            else {
                oldPos = ((LeafNode)temp).getSequence().charAt(temp.getLevel());
            }

            internal.addNode(temp, oldPos);
            temp.setLevel(temp.getLevel() + 1);

            // replace the leafnode with the internal node
            cur.addNode(internal, seq.charAt(cur.getLevel()));

            // set our current node to be temp
            temp = internal;

        }

        return insertHelp(seq, (InternalNode)temp);

    }


    /**
     * Prints out the tree. Basics
     */
    public void print() {
        printHelper(root, 0);
    }


    /**
     * Helper for print
     * 
     * @param cur
     *            - current node
     * @param level
     *            - level of the nodes
     */
    private void printHelper(DNATreeNode cur, int level) {

        String tabs = "";
        for (int i = 0; i < cur.getLevel(); i++) {
            tabs += "  "; // adding two spaced per level
        }

        if (cur instanceof FlyweightNode) {
            for (int i = 0; i < level; i++) {
                tabs += "  "; // adding two spaced per level
            }
            System.out.println(tabs + "E");
        }
        else if (cur instanceof LeafNode) {
            System.out.println(tabs + ((LeafNode)cur).getSequence());
        }
        else {
            System.out.println(tabs + "I");

            DNATreeNode a = ((InternalNode)cur).getNode('A');
            DNATreeNode c = ((InternalNode)cur).getNode('C');
            DNATreeNode g = ((InternalNode)cur).getNode('G');
            DNATreeNode t = ((InternalNode)cur).getNode('T');
            DNATreeNode val = ((InternalNode)cur).getNode('$');

            printHelper(a, cur.getLevel() + 1);
            printHelper(c, cur.getLevel() + 1);
            printHelper(g, cur.getLevel() + 1);
            printHelper(t, cur.getLevel() + 1);
            printHelper(val, cur.getLevel() + 1);

        }
    }


    /**
     * Prints with lengths
     */
    public void printLengths() {
        lenHelper(root, 0);
    }


    /**
     * Helper for printLengths
     * 
     * @param cur
     *            - current node
     * @param level
     *            - current level
     */
    private void lenHelper(DNATreeNode cur, int level) {
        String tabs = "";
        for (int i = 0; i < cur.getLevel(); i++) {
            tabs += "  "; // adding two spaced per level
        }

        if (cur instanceof FlyweightNode) {
            for (int i = 0; i < level; i++) {
                tabs += "  "; // adding two spaced per level
            }
            System.out.println(tabs + "E");
        }
        else if (cur instanceof LeafNode) {
            System.out.println(tabs + ((LeafNode)cur).getSequence() + " "
                + ((LeafNode)cur).getSequence().length());
        }
        else {
            System.out.println(tabs + "I");

            DNATreeNode a = ((InternalNode)cur).getNode('A');
            DNATreeNode c = ((InternalNode)cur).getNode('C');
            DNATreeNode g = ((InternalNode)cur).getNode('G');
            DNATreeNode t = ((InternalNode)cur).getNode('T');
            DNATreeNode val = ((InternalNode)cur).getNode('$');

            lenHelper(a, cur.getLevel() + 1);
            lenHelper(c, cur.getLevel() + 1);
            lenHelper(g, cur.getLevel() + 1);
            lenHelper(t, cur.getLevel() + 1);
            lenHelper(val, cur.getLevel() + 1);

        }
    }


    /**
     * Prints with stats
     */
    public void printStats() {
        statsHelper(root, 0);
    }


    /**
     * Prints with stats
     * 
     * @param cur
     *            - current node
     * @param level
     *            - level of current node
     */
    private void statsHelper(DNATreeNode cur, int level) {
        String tabs = "";
        for (int i = 0; i < cur.getLevel(); i++) {
            tabs += "  "; // adding two spaced per level
        }

        if (cur instanceof FlyweightNode) {
            for (int i = 0; i < level; i++) {
                tabs += "  "; // adding two spaced per level
            }
            System.out.println(tabs + "E");
        }
        else if (cur instanceof LeafNode) {
            System.out.print(tabs + ((LeafNode)cur).getSequence() + " ");
            double len = ((LeafNode)cur).getSequence().length();
            double a = 0;
            double c = 0;
            double g = 0;
            double t = 0;
            for (int i = 0; i < len; i++) {
                int currentChar = ((LeafNode)cur).getSequence().charAt(i);
                switch (currentChar) {
                    case 'A':
                        a++;
                        break;
                    case 'C':
                        c++;
                        break;
                    case 'G':
                        g++;
                        break;
                    case 'T':
                        t++;
                        break;
                    default:
                        break;
                }
            }

            String aPer = String.format("%.2f", a / len * 100);
            String cPer = String.format("%.2f", c / len * 100);
            String gPer = String.format("%.2f", g / len * 100);
            String tPer = String.format("%.2f", t / len * 100);
            System.out.println("A:" + aPer + " C:" + cPer + " G:" + gPer + " T:"
                + tPer);
        }
        else {
            System.out.println(tabs + "I");

            DNATreeNode a = ((InternalNode)cur).getNode('A');
            DNATreeNode c = ((InternalNode)cur).getNode('C');
            DNATreeNode g = ((InternalNode)cur).getNode('G');
            DNATreeNode t = ((InternalNode)cur).getNode('T');
            DNATreeNode val = ((InternalNode)cur).getNode('$');

            statsHelper(a, cur.getLevel() + 1);
            statsHelper(c, cur.getLevel() + 1);
            statsHelper(g, cur.getLevel() + 1);
            statsHelper(t, cur.getLevel() + 1);
            statsHelper(val, cur.getLevel() + 1);

        }
    }


    /**
     * Searches for sequence
     * 
     * @param seq
     *            - sequence to look for
     * @return - int of nodes looked for
     */
    public int search(String seq) {
        // looking for exact
        if (seq.charAt(seq.length() - 1) == '$') {
            seq = seq.substring(0, seq.length() - 1);
            if (root instanceof FlyweightNode) {
                System.out.println("# of nodes visited: " + 1);
                System.out.println("no sequence found");
                return -1;
            }
            else if (root instanceof LeafNode) {
                if (((LeafNode)root).getSequence().equals(seq)) {
                    System.out.println("# of nodes visited: " + 1);
                    System.out.println("sequence: " + ((LeafNode)root)
                        .getSequence());
                    return 1;
                }
                else {
                    System.out.println("# of nodes visited: " + 1);
                    System.out.println("no sequence found");
                    return -1;
                }
            }
            else {
                return searchExact(seq, (InternalNode)root, 1);
            }

        }
        // prefix
        else {
            if (root instanceof FlyweightNode) {
                System.out.println("# of nodes visited: " + 1);
                System.out.println("no sequence found");
                return -1;
            }
            else if (root instanceof LeafNode) {
                if (((LeafNode)root).getSequence().startsWith(seq)) {
                    // print matching seq, we're done
                    System.out.println("# of nodes visited: " + 1);
                    System.out.println("sequence: " + ((LeafNode)root)
                        .getSequence());
                    return 1;
                }
                else {
                    System.out.println("# of nodes visited: " + 1);
                    System.out.println("no sequence found");
                    return -1;
                }

            }
            else {
                // internal node
                StringBuilder sb = new StringBuilder();
                return searchPre(seq, 1, (InternalNode)root, sb);
            }
        }

        // need to find out when it actually hits this.

    }


    /**
     * Search for exact sequence
     * 
     * @param seq
     *            - sequence to look for
     * @param cur
     *            - current node
     * @param count
     *            - number or nodes visited
     * @return number of nodes visited
     */
    public int searchExact(String seq, InternalNode cur, int count) {
        char pos; // the pos will allow us to know where to add the new seq
        if (cur.getLevel() == seq.length()) {
            pos = '$';
        }
        else {
            pos = seq.charAt(cur.getLevel());
        }

        DNATreeNode temp = ((InternalNode)cur).getNode(pos);

        if (temp instanceof FlyweightNode) {
            count++;
            System.out.println("# of nodes visited: " + count);
            System.out.println("no sequence found");
            return -1; // not found
        }

        else if (temp instanceof LeafNode) {
            // found
            if (((LeafNode)temp).getSequence().equals(seq)) {
                count++;
                System.out.println("# of nodes visited: " + count);
                System.out.println("sequence: " + seq);
                return count;
            }
            else {
                count++;
                System.out.println("# of nodes visited: " + count);
                System.out.println("no sequence found");
                return -1;
            }
        }

        // internal node
        return searchExact(seq, (InternalNode)temp, count + 1);

    }


    /**
     * We know that root will be an internal node
     * 
     * @param seq
     *            the seq we lookin for
     * @param count
     *            number of nodes visited
     * @param cur
     *            the current node in the recursive chain
     * @param sb
     *            a stringbuilder for the search function
     * @return number of nodes visited
     */
    public int searchPre(
        String seq,
        int count,
        InternalNode cur,
        StringBuilder sb) {
        char pos; // the pos will allow us to know where to search for seq

        if (cur.getLevel() >= seq.length()) {
            // once we hit past level, we gotta check everything
            count += checkEverything(seq, cur, sb);
            System.out.println("# of nodes visited: " + count);
            System.out.print(sb.toString());
            return count;
        }

        else {
            pos = seq.charAt(cur.getLevel());
            DNATreeNode temp = ((InternalNode)cur).getNode(pos);

            if (temp instanceof LeafNode) {
                // found
                if (((LeafNode)temp).getSequence().startsWith(seq)) {
                    // print matching seq, we're done
                    count++;
                    System.out.println("# of nodes visited: " + count);
                    System.out.println("sequence: " + ((LeafNode)temp)
                        .getSequence());
                    return count;
                }
                // no match also means we're done
                else {
                    count++;
                    System.out.println("# of nodes visited: " + count);
                    System.out.println("no sequence found");
                    return -1;
                }
            }
            else if (temp instanceof InternalNode) {
                // internal node, keep going
                return searchPre(seq, count + 1, (InternalNode)temp, sb);
            }
            // flyweight, done
            else {
                count++;
                System.out.println("# of nodes visited: " + count);
                System.out.println("no sequence found");
                return -1;
            }
        }

    }


    /**
     * process to go through all internal nodes when level is passed
     * 
     * @param seq
     *            - seq to look for
     * @param cur
     *            - current node
     * @param sb
     *            - string builder to properly create printout
     * @return - nodes visited
     */
    public int checkEverything(String seq, InternalNode cur, StringBuilder sb) {
        int count = 5; // default at 5 checks

        DNATreeNode a = ((InternalNode)cur).getNode('A');
        DNATreeNode c = ((InternalNode)cur).getNode('C');
        DNATreeNode g = ((InternalNode)cur).getNode('G');
        DNATreeNode t = ((InternalNode)cur).getNode('T');
        DNATreeNode val = ((InternalNode)cur).getNode('$');

        // check a
        if (a instanceof LeafNode) {
            if (((LeafNode)a).getSequence().startsWith(seq)) {
                // print matching seq

                sb.append("sequence: " + ((LeafNode)a).getSequence() + "\n");
            }
        }
        else if (a instanceof InternalNode) {
// count--;
            count += checkEverything(seq, (InternalNode)a, sb);
        }

        // check c
        if (c instanceof LeafNode) {
            if (((LeafNode)c).getSequence().startsWith(seq)) {
                // print matching seq
                sb.append("sequence: " + ((LeafNode)c).getSequence() + "\n");
            }
        }

        else if (c instanceof InternalNode) {
// count--;
            count += checkEverything(seq, (InternalNode)c, sb);
        }

        // check g
        if (g instanceof LeafNode) {
            if (((LeafNode)g).getSequence().startsWith(seq)) {
                // print matching seq
                sb.append("sequence: " + ((LeafNode)g).getSequence() + "\n");
            }
        }

        else if (g instanceof InternalNode) {
// count--;
            count += checkEverything(seq, (InternalNode)g, sb);
        }

        // check t
        if (t instanceof LeafNode) {
            if (((LeafNode)t).getSequence().startsWith(seq)) {
                // print matching seq
                sb.append("sequence: " + ((LeafNode)t).getSequence() + "\n");
            }
        }

        else if (t instanceof InternalNode) {
// count--;
            count += checkEverything(seq, (InternalNode)t, sb);
        }

        // check $
        if (val instanceof LeafNode && ((LeafNode)val).getSequence().startsWith(
            seq)) {

            // print matching seq
            sb.append("sequence: " + ((LeafNode)val).getSequence() + "\n");

        }

        return count;
    }


    /**
     * Removes node
     * 
     * @param seq
     *            - sequence to look for
     * @return - if the seq got removed
     */
    public boolean remove(String seq) {
        // first find
        if (root instanceof FlyweightNode) {
            return false;
        }
        else if (root instanceof LeafNode) {
            if (((LeafNode)root).getSequence().equals(seq)) {
                root = fw;
                return true;
            }
            else {
                return false;
            }
        }

        boolean res = removeHelp(seq, (InternalNode)root);

        if (!res) {
            return false;
        }
        else {
            // check for 4 fly weight
            if (root instanceof InternalNode) {
                DNATreeNode a = ((InternalNode)root).getNode('A');
                DNATreeNode c = ((InternalNode)root).getNode('C');
                DNATreeNode g = ((InternalNode)root).getNode('G');
                DNATreeNode t = ((InternalNode)root).getNode('T');
                DNATreeNode val = ((InternalNode)root).getNode('$');

                DNATreeNode[] array = { a, c, g, t, val };
                int flyweights = 0;
                DNATreeNode newNode = null;

                for (int i = 0; i < array.length; i++) {
                    DNATreeNode node = array[i];
                    if (node instanceof FlyweightNode) {
                        flyweights++;
                    }
                    else {
                        newNode = node;
                    }
                }

                if (flyweights == 4 && newNode instanceof LeafNode) {
                    root = newNode;
                    root.setLevel(0);
                }
            }
            return res;
        }
    }


    /**
     * Helps the remove method
     * 
     * @param seq
     *            - seq to remove
     * @param dnaNode
     *            - current node
     * @return - if the node was removed
     */
    public boolean removeHelp(String seq, DNATreeNode dnaNode) {

        InternalNode cur = (InternalNode)dnaNode;

        char pos = ' '; // the pos will allow us to know where to add the new
                        // seq
        if (cur.getLevel() >= seq.length()) {
            pos = '$';
        }
        else {
            try {
                pos = seq.charAt(cur.getLevel());
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }

        // the next node
        DNATreeNode temp = cur.getNode(pos);

        // dead end
        if (temp instanceof FlyweightNode) {
            return false;
        }
        // case for if there is only one other Node
        else if (temp instanceof LeafNode) {
            // remove node
            if (((LeafNode)temp).getSequence().equals(seq)) {
                cur.addNode(fw, pos);

                DNATreeNode a = ((InternalNode)cur).getNode('A');
                DNATreeNode c = ((InternalNode)cur).getNode('C');
                DNATreeNode g = ((InternalNode)cur).getNode('G');
                DNATreeNode t = ((InternalNode)cur).getNode('T');
                DNATreeNode val = ((InternalNode)cur).getNode('$');

                DNATreeNode[] array = { a, c, g, t, val };
                int flyweights = 0;
                DNATreeNode newNode = null;

                for (int i = 0; i < array.length; i++) {
                    DNATreeNode node = array[i];
                    if (node instanceof FlyweightNode) {
                        flyweights++;
                    }
                    else {
                        newNode = node;
                    }
                }

                if (flyweights == 4 && newNode instanceof LeafNode) {
                    temp = newNode;
                    // temp.setLevel(temp.getLevel()-1);
                }

                return true;
            }
            // dead end
            else {
                return false;
            }
        }

        if (removeHelp(seq, (InternalNode)temp)) {

            DNATreeNode a = ((InternalNode)temp).getNode('A');
            DNATreeNode c = ((InternalNode)temp).getNode('C');
            DNATreeNode g = ((InternalNode)temp).getNode('G');
            DNATreeNode t = ((InternalNode)temp).getNode('T');
            DNATreeNode val = ((InternalNode)temp).getNode('$');

            DNATreeNode[] array = { a, c, g, t, val };
            int flyweights = 0;
            DNATreeNode newNode = null;

            for (int i = 0; i < array.length; i++) {
                DNATreeNode node = array[i];
                if (node instanceof FlyweightNode) {
                    flyweights++;
                }
                else {
                    newNode = node;
                }
            }

            if (flyweights == 4 && newNode instanceof LeafNode) {
                newNode.setLevel(newNode.getLevel() - 1);
                cur.addNode(newNode, pos);

            }

            return true;

        }

        return removeHelp(seq, (InternalNode)temp);

// return removeHelp(seq, (InternalNode)temp);
    }

}
