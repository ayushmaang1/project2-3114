/**
 * Internal Node has all the nodes in the tree which are internal.
 * 
 * @author Bryan Covell (bpc6), Ayush Ganotra (ayushg)
 * @version 2020-03-13
 *
 */
public class InternalNode extends DNATreeNode {

    private DNATreeNode[] leaves = new DNATreeNode[5];


    /**
     * Constructor to create Internal Node
     * 
     * @param fw
     *            - flyweight node
     * @param level
     *            - level of the node
     */
    public InternalNode(FlyweightNode fw, int level) {
        leaves[0] = fw;
        leaves[1] = fw;
        leaves[2] = fw;
        leaves[3] = fw;
        leaves[4] = fw;
        setLevel(level);
    }


    /**
     * Add node to leaves
     * 
     * @param node
     *            - node to add
     * @param pos
     *            - the position to add
     */
    public void addNode(DNATreeNode node, char pos) {
        switch (pos) {
            case 'A':
                leaves[0] = node;
                break;
            case 'C':
                leaves[1] = node;
                break;
            case 'T':
                leaves[2] = node;
                break;
            case 'G':
                leaves[3] = node;
                break;
            default:
                leaves[4] = node;
                break;
        }
    }


    /**
     * Gets a node from the position
     * 
     * @param pos
     *            - position of the node
     * @return - the node in that pos
     */
    public DNATreeNode getNode(char pos) {
        switch (pos) {
            case 'A':
                return leaves[0];
            case 'C':
                return leaves[1];
            case 'T':
                return leaves[2];
            case 'G':
                return leaves[3];
            default:
                return leaves[4];
        }
    }

}
