import java.io.FileNotFoundException;
import student.TestCase;

/**
 * Test class for the project driver, DNAtree.
 * 
 * @author Bryan Covell (bpc6), Ayush Ganotra (ayushg)
 * @version March 09, 2020
 */
public class DNAtreeTest extends TestCase {

    /**
     * Tests if file does not exist
     */
    public void testMain() {
        boolean error = false;
        try {
            String[] args = { "notAFile" };
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            error = true;
        }
        assertTrue(error);
    }


    /**
     * Tests the insert command as described in the specs
     */
    public void testInsert() {
        String[] args = { "input.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence ACGT inserted at level 0\n");
        expected.append("sequence AAAA inserted at level 2\n");
        expected.append("sequence AA inserted at level 3\n");
        expected.append("sequence AAACCCCGGTGAAAACGTA inserted at level 4\n");
        expected.append("sequence ACTGGGAA inserted at level 3\n");

        expected.append("sequence BBBBB is invalid\n");
        expected.append("sequence 9 is invalid\n");

        expected.append("sequence AA already exists\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Tests the remove command as described in the specs
     */
    public void testRemove() {
        String[] args = { "remove.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence A inserted at level 0\n");
        expected.append("sequence B is invalid\n");
        expected.append("sequence C does not exist\n");
        expected.append("sequence A removed\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Tests the search command as described in the specs
     */
    public void testSearch() {
        String[] args = { "search.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence A inserted at level 0\n");
        expected.append("sequence T inserted at level 1\n");
        expected.append("sequence AA inserted at level 2\n");

        expected.append("sequence B is invalid\n");
        expected.append("# of nodes visited: 2\n" + "no sequence found\n");
        expected.append("# of nodes visited: 3\n" + "no sequence found\n");
        expected.append("# of nodes visited: 3\n" + "sequence: A\n");
        expected.append("# of nodes visited: 7\n" + "sequence: AA\n"
            + "sequence: A\n"); // order of the last 2 nodes
                                // may be switched

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Tests the print command as described in the specs
     */
    public void testPrint() {
        String[] args = { "print.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence AC inserted at level 0\n");
        expected.append("sequence GCT inserted at level 1\n");

        expected.append("tree dump:\n" + "I\n  AC\n  E\n  GCT\n  E\n  E\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Tests the print lengths command as described in the specs
     */
    public void testPrintLengths() {
        String[] args = { "print_lengths.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence AC inserted at level 0\n");
        expected.append("sequence GCT inserted at level 1\n");

        expected.append("tree dump:\r\n" + "I\r\n" + "  AC 2\r\n" + "  E\r\n"
            + "  GCT 3\r\n" + "  E\r\n" + "  E\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Tests the print stats command as described in the specs
     */
    public void testPrintStats() {
        String[] args = { "print_stats.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence AC inserted at level 0\n");
        expected.append("sequence GCT inserted at level 1\n");

        expected.append("tree dump:\r\n" + "I\r\n"
            + "  AC A:50.00 C:50.00 G:0.00 T:0.00\r\n" + "  E\r\n"
            + "  GCT A:0.00 C:33.33 G:33.33 T:33.33\r\n" + "  E\r\n" + "  E\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Tests all the commands using the shorter provided test script
     */
    public void testProvided1() {
        String[] args = { "provided1.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence ACGT inserted at level 0\r\n"
            + "sequence AAAA inserted at level 2\r\n"
            + "sequence AA inserted at level 3\r\n"
            + "sequence AAACCCCGGTGAAAACGTA inserted at level 4\r\n"
            + "sequence ACTGGGAA inserted at level 3\r\n"
            + "sequence ACGT removed\r\n"
            + "sequence ACCTT inserted at level 3\r\n"
            + "sequence ACTTA inserted at level 4\r\n" + "tree dump:\r\n"
            + "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA\r\n" + "        AAACCCCGGTGAAAACGTA\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA\r\n"
            + "    I\r\n" + "      E\r\n" + "      ACCTT\r\n" + "      E\r\n"
            + "      I\r\n" + "        E\r\n" + "        E\r\n"
            + "        ACTGGGAA\r\n" + "        ACTTA\r\n" + "        E\r\n"
            + "      E\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "  E\r\n" + "  E\r\n" + "  E\r\n"
            + "sequence TATA inserted at level 1\r\n"
            + "sequence TCG inserted at level 2\r\n" + "tree dump:\r\n"
            + "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA 4\r\n" + "        AAACCCCGGTGAAAACGTA 19\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA 2\r\n"
            + "    I\r\n" + "      E\r\n" + "      ACCTT 5\r\n" + "      E\r\n"
            + "      I\r\n" + "        E\r\n" + "        E\r\n"
            + "        ACTGGGAA 8\r\n" + "        ACTTA 5\r\n" + "        E\r\n"
            + "      E\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "  E\r\n" + "  I\r\n" + "    TATA 4\r\n"
            + "    TCG 3\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "tree dump:\r\n" + "I\r\n" + "  I\r\n" + "    I\r\n"
            + "      I\r\n" + "        AAAA A:100.00 C:0.00 G:0.00 T:0.00\r\n"
            + "        AAACCCCGGTGAAAACGTA A:42.11 C:26.32 G:21.05 T:10.53\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n"
            + "      AA A:100.00 C:0.00 G:0.00 T:0.00\r\n" + "    I\r\n"
            + "      E\r\n" + "      ACCTT A:20.00 C:40.00 G:0.00 T:40.00\r\n"
            + "      E\r\n" + "      I\r\n" + "        E\r\n" + "        E\r\n"
            + "        ACTGGGAA A:37.50 C:12.50 G:37.50 T:12.50\r\n"
            + "        ACTTA A:40.00 C:20.00 G:0.00 T:40.00\r\n"
            + "        E\r\n" + "      E\r\n" + "    E\r\n" + "    E\r\n"
            + "    E\r\n" + "  E\r\n" + "  E\r\n" + "  I\r\n"
            + "    TATA A:50.00 C:0.00 G:0.00 T:50.00\r\n"
            + "    TCG A:0.00 C:33.33 G:33.33 T:33.33\r\n" + "    E\r\n"
            + "    E\r\n" + "    E\r\n" + "  E\r\n"
            + "# of nodes visited: 5\r\n" + "sequence: AAAA\r\n"
            + "# of nodes visited: 13\r\n" + "sequence: AAAA\r\n"
            + "sequence: AAACCCCGGTGAAAACGTA\r\n" + "sequence: AA\r\n"
            + "# of nodes visited: 4\r\n" + "no sequence found\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Tests the print stats command as described in the specs
     */
    public void testProvided2() {
        String[] args = { "provided2.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence ACGT inserted at level 0\n"); // line 1
        expected.append("tree dump:\nACGT\n"); // 2
        expected.append("sequence AAAA inserted at level 2\n"); // 3
        expected.append("tree dump:\r\n" + // 4
            "I\r\n" + "  I\r\n" + "    AAAA\r\n" + "    ACGT\r\n" + "    E\r\n"
            + "    E\r\n" + "    E\r\n" + "  E\r\n" + "  E\r\n" + "  E\r\n"
            + "  E\n");
        expected.append("sequence AA inserted at level 3\n"); // 5
        expected.append("tree dump:\r\n" + // 6
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      AAAA\r\n" + "      E\r\n"
            + "      E\r\n" + "      E\r\n" + "      AA\r\n" + "    ACGT\r\n"
            + "    E\r\n" + "    E\r\n" + "    E\r\n" + "  E\r\n" + "  E\r\n"
            + "  E\r\n" + "  E\n");
        expected.append("sequence AAACCCCGGTGAAAACGTA inserted at level 4\n");
        expected.append("tree dump:\r\n" + // 8
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA\r\n" + "        AAACCCCGGTGAAAACGTA\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA\r\n"
            + "    ACGT\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "  E\r\n" + "  E\r\n" + "  E\n");
        expected.append("sequence ACTGGGAA inserted at level 3\n"); // 9
        expected.append("tree dump:\r\n" + // 10
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA\r\n" + "        AAACCCCGGTGAAAACGTA\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA\r\n"
            + "    I\r\n" + "      E\r\n" + "      E\r\n" + "      ACGT\r\n"
            + "      ACTGGGAA\r\n" + "      E\r\n" + "    E\r\n" + "    E\r\n"
            + "    E\r\n" + "  E\r\n" + "  E\r\n" + "  E\r\n" + "  E\n");
        expected.append("sequence ACGT removed\n"); // 11
        expected.append("tree dump:\r\n" + // 12
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA\r\n" + "        AAACCCCGGTGAAAACGTA\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA\r\n"
            + "    ACTGGGAA\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "  E\r\n" + "  E\r\n" + "  E\n");
        expected.append("sequence ACCTT inserted at level 3\n"); // 13
        expected.append("tree dump:\r\n" + // 14
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA\r\n" + "        AAACCCCGGTGAAAACGTA\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA\r\n"
            + "    I\r\n" + "      E\r\n" + "      ACCTT\r\n" + "      E\r\n"
            + "      ACTGGGAA\r\n" + "      E\r\n" + "    E\r\n" + "    E\r\n"
            + "    E\r\n" + "  E\r\n" + "  E\r\n" + "  E\r\n" + "  E\n");
        expected.append("sequence ACTTA inserted at level 4\n"); // 15
        expected.append("tree dump:\r\n" + // 16
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA\r\n" + "        AAACCCCGGTGAAAACGTA\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA\r\n"
            + "    I\r\n" + "      E\r\n" + "      ACCTT\r\n" + "      E\r\n"
            + "      I\r\n" + "        E\r\n" + "        E\r\n"
            + "        ACTGGGAA\r\n" + "        ACTTA\r\n" + "        E\r\n"
            + "      E\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "  E\r\n" + "  E\r\n" + "  E\n");
        expected.append("sequence TATA inserted at level 1\n"); // 17
        expected.append("tree dump:\r\n" + // 18
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA\r\n" + "        AAACCCCGGTGAAAACGTA\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA\r\n"
            + "    I\r\n" + "      E\r\n" + "      ACCTT\r\n" + "      E\r\n"
            + "      I\r\n" + "        E\r\n" + "        E\r\n"
            + "        ACTGGGAA\r\n" + "        ACTTA\r\n" + "        E\r\n"
            + "      E\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "  E\r\n" + "  TATA\r\n" + "  E\n");
        expected.append("sequence TCG inserted at level 2\n"); // 19
        expected.append("tree dump:\r\n" + // 20
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA\r\n" + "        AAACCCCGGTGAAAACGTA\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA\r\n"
            + "    I\r\n" + "      E\r\n" + "      ACCTT\r\n" + "      E\r\n"
            + "      I\r\n" + "        E\r\n" + "        E\r\n"
            + "        ACTGGGAA\r\n" + "        ACTTA\r\n" + "        E\r\n"
            + "      E\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "  E\r\n" + "  I\r\n" + "    TATA\r\n" + "    TCG\r\n"
            + "    E\r\n" + "    E\r\n" + "    E\r\n" + "  E\n");
        expected.append("tree dump:\r\n" + // 21
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA 4\r\n" + "        AAACCCCGGTGAAAACGTA 19\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n" + "      AA 2\r\n"
            + "    I\r\n" + "      E\r\n" + "      ACCTT 5\r\n" + "      E\r\n"
            + "      I\r\n" + "        E\r\n" + "        E\r\n"
            + "        ACTGGGAA 8\r\n" + "        ACTTA 5\r\n" + "        E\r\n"
            + "      E\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\r\n" + "  E\r\n" + "  I\r\n" + "    TATA 4\r\n"
            + "    TCG 3\r\n" + "    E\r\n" + "    E\r\n" + "    E\r\n"
            + "  E\n");
        expected.append("tree dump:\r\n" + // 22
            "I\r\n" + "  I\r\n" + "    I\r\n" + "      I\r\n"
            + "        AAAA A:100.00 C:0.00 G:0.00 T:0.00\r\n"
            + "        AAACCCCGGTGAAAACGTA A:42.11 C:26.32 G:21.05 T:10.53\r\n"
            + "        E\r\n" + "        E\r\n" + "        E\r\n"
            + "      E\r\n" + "      E\r\n" + "      E\r\n"
            + "      AA A:100.00 C:0.00 G:0.00 T:0.00\r\n" + "    I\r\n"
            + "      E\r\n" + "      ACCTT A:20.00 C:40.00 G:0.00 T:40.00\r\n"
            + "      E\r\n" + "      I\r\n" + "        E\r\n" + "        E\r\n"
            + "        ACTGGGAA A:37.50 C:12.50 G:37.50 T:12.50\r\n"
            + "        ACTTA A:40.00 C:20.00 G:0.00 T:40.00\r\n"
            + "        E\r\n" + "      E\r\n" + "    E\r\n" + "    E\r\n"
            + "    E\r\n" + "  E\r\n" + "  E\r\n" + "  I\r\n"
            + "    TATA A:50.00 C:0.00 G:0.00 T:50.00\r\n"
            + "    TCG A:0.00 C:33.33 G:33.33 T:33.33\r\n" + "    E\r\n"
            + "    E\r\n" + "    E\r\n" + "  E\n");
        expected.append("# of nodes visited: 5\r\n" + // 23
            "sequence: AAAA\n");
        expected.append("# of nodes visited: 13\r\n" + // 24
            "sequence: AAAA\r\n" + "sequence: AAACCCCGGTGAAAACGTA\r\n"
            + "sequence: AA\n");
        expected.append("# of nodes visited: 4\r\n" + // 25
            "no sequence found\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Edge case testing for insert
     */
    public void testInsert2() {
        String[] args = { "insert2.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence AC inserted at level 0\n");
        expected.append("sequence ACG inserted at level 3\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Edge case testing for remove
     */
    public void testRemove2() {
        String[] args = { "remove2.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence AC inserted at level 0\n");
        expected.append("sequence ACG inserted at level 3\n");
        expected.append("sequence AC removed\n");
        expected.append("tree dump:\nACG\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }


    /**
     * Test another edge case for remove
     */
    public void testRemove3() {
        String[] args = { "remove3.txt" };
        try {
            DNAtree.main(args);
        }
        catch (FileNotFoundException e) {
            // Do nothing
        }
        StringBuilder expected = new StringBuilder();
        expected.append("sequence AC inserted at level 0\n");
        expected.append("sequence A inserted at level 2\n");
        expected.append("sequence T inserted at level 1\n");
        expected.append("sequence T removed\n");

        String actual = systemOut().getHistory();
        assertEquals(expected.toString(), actual);
    }

}
